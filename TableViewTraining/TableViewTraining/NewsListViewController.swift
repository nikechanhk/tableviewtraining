//
//  NewsListViewController.swift
//  TableViewTraining
//
//  Created by CHAN LAI KIT on 11/4/2018.
//  Copyright © 2018年 Accenture. All rights reserved.
//

import UIKit

class NewsListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var mainTableView: UITableView!
    
    var hkNewsArray: [NewsItem]?
    var sportNewsArray: [NewsItem]?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.mainTableView.register(UINib.init(nibName: "NewsItemTableViewCell", bundle: nil), forCellReuseIdentifier: String(describing: NewsItemTableViewCell.self))
        self.mainTableView.register(UINib.init(nibName: "ImageNewsItemTableViewCell", bundle: nil), forCellReuseIdentifier: String(describing: ImageNewsItemTableViewCell.self))
        self.mainTableView.register(UINib.init(nibName: "NewsSectionHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: String(describing: NewsSectionHeaderView.self))
        self.mainTableView.estimatedRowHeight = 120.0
        
        self.hkNewsArray = DataStore.sharedInstance.getHKNewsFeed()
        self.sportNewsArray = DataStore.sharedInstance.getSportNewsFeed()
        
        Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(self.refreshHKNews), userInfo: nil, repeats: false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Timer action
    @objc func refreshHKNews() {
        self.hkNewsArray = DataStore.sharedInstance.getHKNewsV2Feed()
        self.mainTableView.reloadData()
    }
    
    // MARK: - UITableViewDataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return (self.hkNewsArray?.count)!
        }
        else {
            return self.sportNewsArray!.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var newsItem: NewsItem!
        
        if indexPath.section == 0 {
            newsItem = self.hkNewsArray![indexPath.row]
        }
        else {
            newsItem = self.sportNewsArray![indexPath.row]
        }
        
        if newsItem.hasImage == true {
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ImageNewsItemTableViewCell.self)) as! ImageNewsItemTableViewCell
            cell.newsTitleLabel.text = newsItem.newsTitle
            cell.timeLabel.text = newsItem.timeString
            cell.newsContentLabel.text = newsItem.newsContent
            cell.newsImageView.image = UIImage.init(named: "news_image")
            
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: NewsItemTableViewCell.self)) as! NewsItemTableViewCell
            cell.newsTitleLabel.text = newsItem.newsTitle
            cell.timeLabel.text = newsItem.timeString
            cell.newsContentLabel.text = newsItem.newsContent
            
            return cell
        }
    }
    
    // MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let newsItem = self.hkNewsArray![indexPath.row]
        
        if newsItem.isActive == true {
            return UITableViewAutomaticDimension
        }
        else {
            return CGFloat.leastNormalMagnitude
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: String(describing: NewsSectionHeaderView.self)) as! NewsSectionHeaderView
        
        if section == 0 {
            headerView.sectionTitleLabel.text = "港聞"
        }
        else {
            headerView.sectionTitleLabel.text = "體育"
        }
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40.0
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
