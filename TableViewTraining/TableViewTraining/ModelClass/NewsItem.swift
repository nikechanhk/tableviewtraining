//
//  NewsItem.swift
//  TableViewTraining
//
//  Created by Chan, Nike on 11/4/2018.
//  Copyright © 2018 Accenture. All rights reserved.
//

import Foundation

struct NewsItem {
    var newsTitle: String = ""
    var timeString: String = ""
    var newsContent: String = ""
    var hasImage: Bool = false
    var isActive: Bool = true
    
    init(json: JSON) {
        self.newsTitle = json["title"].string ?? ""
        self.timeString = json["time_string"].string ?? ""
        self.newsContent = json["news_content"].string ?? ""
        self.hasImage = json["has_image"].bool ?? false
        self.isActive = json["is_active"].bool ?? true
    }
}
