//
//  NewsItemTableViewCell.swift
//  TableViewTraining
//
//  Created by Chan, Nike on 11/4/2018.
//  Copyright © 2018 Accenture. All rights reserved.
//

import UIKit

class NewsItemTableViewCell: UITableViewCell {
    @IBOutlet weak var newsTitleLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var newsContentLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
