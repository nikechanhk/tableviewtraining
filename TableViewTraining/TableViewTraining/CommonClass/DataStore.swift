//
//  DataStore.swift
//  TableViewTraining
//
//  Created by Chan, Nike on 11/4/2018.
//  Copyright © 2018 Accenture. All rights reserved.
//

import UIKit

class DataStore: NSObject {
    static let sharedInstance = DataStore()
    private override init() {}
    
    func getHKNewsFeed() -> [NewsItem] {
        if let path = Bundle.main.path(forResource: "NewsList", ofType: "json") {
            do {
                let jsonData = try Data.init(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let newsJsonArray = try JSON.init(data: jsonData).arrayValue
                
                var newsArray = [NewsItem]()
                for json in newsJsonArray {
                    let newsItem = NewsItem.init(json: json)
                    newsArray.append(newsItem)
                }
                
                return newsArray
            }
            catch {
                return []
            }
            
        }
        else {
            return []
        }
    }
    
    func getSportNewsFeed() -> [NewsItem] {
        if let path = Bundle.main.path(forResource: "SportList", ofType: "json") {
            do {
                let jsonData = try Data.init(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let newsJsonArray = try JSON.init(data: jsonData).arrayValue
                
                var newsArray = [NewsItem]()
                for json in newsJsonArray {
                    let newsItem = NewsItem.init(json: json)
                    newsArray.append(newsItem)
                }
                
                return newsArray
            }
            catch {
                return []
            }
            
        }
        else {
            return []
        }
    }
    
    func getHKNewsV2Feed() -> [NewsItem] {
        if let path = Bundle.main.path(forResource: "NewsList_v2", ofType: "json") {
            do {
                let jsonData = try Data.init(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let newsJsonArray = try JSON.init(data: jsonData).arrayValue
                
                var newsArray = [NewsItem]()
                for json in newsJsonArray {
                    let newsItem = NewsItem.init(json: json)
                    newsArray.append(newsItem)
                }
                
                return newsArray
            }
            catch {
                return []
            }
            
        }
        else {
            return []
        }
    }
}
