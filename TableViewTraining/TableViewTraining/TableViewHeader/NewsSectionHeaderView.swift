//
//  NewsSectionHeaderView.swift
//  TableViewTraining
//
//  Created by CHAN LAI KIT on 11/4/2018.
//  Copyright © 2018年 Accenture. All rights reserved.
//

import UIKit

class NewsSectionHeaderView: UITableViewHeaderFooterView {
    @IBOutlet weak var sectionTitleLabel: UILabel!
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
